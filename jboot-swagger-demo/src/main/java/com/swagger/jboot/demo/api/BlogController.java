package com.swagger.jboot.demo.api;

import com.jfinal.kit.Kv;
import com.lastb7.swagger.annotation.ApiRes;
import com.lastb7.swagger.annotation.ApiResProperty;

import io.jboot.web.controller.JbootController;
import io.jboot.web.controller.annotation.RequestMapping;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * @author: lbq
 * @email: 526509994@qq.com
 * @date: 2021/7/12
 */
@RequestMapping("${apiPath}/blog")
@Api(description = "HelloWorld", tags = "最简用法")
public class BlogController extends JbootController {


    @ApiOperation(value = "简单返回值", notes = "SwaggerConst.COMMON_RES.data中返回值")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "paramA", value = "参数a", defaultValue = "1111"),
            @ApiImplicitParam(name = "paramB", value = "参数b", defaultValue = "222"),
    })
    @ApiRes({
            @ApiResProperty(name = "resA", value = "返回值A", example = "hello word1"),
            @ApiResProperty(name = "resB", value = "返回值b", example = "hello word2"),
    })
    public void test() {
        Kv kv = new Kv();
        kv.put("resA", "1111111");
        kv.put("resB", "222222");

        this.renderJson(kv);
    }
}
