package com.swagger.jboot.demo;

import com.jfinal.config.Constants;
import com.jfinal.config.Interceptors;
import com.jfinal.config.Routes;
import com.jfinal.template.Engine;
import com.lastb7.swagger.handler.SwaggerHandler;
import com.lastb7.swagger.plugin.SwaggerPlugin;
import com.lastb7.swagger.routes.SwaggerRoutes;

import io.jboot.aop.jfinal.JfinalHandlers;
import io.jboot.aop.jfinal.JfinalPlugins;
import io.jboot.app.JbootApplication;
import io.jboot.core.listener.JbootAppListener;

/**
 * 启动入口
 *
 * @author: lbq
 * @email: 526509994@qq.com
 * @date: 2021/7/12
 */
public class AppConfig implements JbootAppListener {

    public static void main(String[] args) {
        JbootApplication.createServer(args)
                .addHotSwapClassPrefix("com.lastb7.swagger.")
                .start();
    }

    @Override
    public void onInit() {

    }

    @Override
    public void onConstantConfig(Constants constants) {

    }

    @Override
    public void onRouteConfig(Routes me) {
        // swagger路由
        me.add(new SwaggerRoutes());

    }

    @Override
    public void onEngineConfig(Engine engine) {

    }

    @Override
    public void onPluginConfig(JfinalPlugins me) {
        me.add(new SwaggerPlugin());
    }

    @Override
    public void onInterceptorConfig(Interceptors interceptors) {

    }

    @Override
    public void onHandlerConfig(JfinalHandlers me) {
        me.add(new SwaggerHandler());
    }

    @Override
    public void onStartBefore() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStartFinish() {

    }

    @Override
    public void onStop() {

    }
}
