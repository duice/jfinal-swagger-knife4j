package com.lastb7.swagger.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.jfinal.handler.Handler;
import com.jfinal.kit.LogKit;
import com.lastb7.swagger.util.IOUtils;

/**
 * 访问Jar资源
 *
 * @author: lbq
 * 联系方式: 526509994@qq.com
 * 创建日期: 2020/6/11
 */
public class SwaggerHandler extends Handler {
    @Override
    public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled) {
        if (target.contains("webjars/")) {
            String path = "META-INF/resources" + target;

            InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(path);
            OutputStream outputStream = null;
            try {
                if (inputStream != null) {
                    outputStream = response.getOutputStream();
                    IOUtils.copy(inputStream, response.getOutputStream());
                }
            } catch (IOException e) {
                LogKit.error("无法读取Swagger UI静态资源", e);
            } finally {
                IOUtils.closeQuietly(inputStream);
                IOUtils.closeQuietly(outputStream);
            }
            isHandled[0] = true;
        } else {
            this.next.handle(target, request, response, isHandled);
        }
    }
}
