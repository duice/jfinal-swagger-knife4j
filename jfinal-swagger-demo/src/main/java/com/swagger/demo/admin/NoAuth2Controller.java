package com.swagger.demo.admin;

import com.jfinal.core.Controller;
import com.jfinal.core.Path;
import com.lastb7.swagger.annotation.ApiNoAuthorize;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * @author: lbq
 * 联系方式: 526509994@qq.com
 * 创建日期: 2020/9/16
 */
@Path("/noAuthMethod")
@ApiNoAuthorize
@Api(tags = "Ctrl接口不校验")
public class NoAuth2Controller extends Controller {

    @ApiOperation(value = "test1不验证权限", notes = "SwaggerConst.COMMON_RES定义返回")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "paramA", value = "参数a", defaultValue = "1111"),
            @ApiImplicitParam(name = "paramB", value = "参数b", defaultValue = "222"),
    })
    public void test1() {
        this.renderJson();
    }

    @ApiOperation(value = "test2不验证权限", notes = "SwaggerConst.COMMON_RES定义返回")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "paramA", value = "参数a", defaultValue = "1111"),
            @ApiImplicitParam(name = "paramB", value = "参数b", defaultValue = "222"),
    })
    public void test2() {
        this.renderJson();
    }

}
