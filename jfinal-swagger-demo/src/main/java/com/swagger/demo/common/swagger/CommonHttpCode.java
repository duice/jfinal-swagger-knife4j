//package com.swagger.demo.common.swagger;
//
//import com.jfinal.kit.Okv;
//import com.lastb7.swagger.annotation.ApiModel;
//
///**
// * @author: lbq
// * 联系方式: 526509994@qq.com
// * 创建日期: 2020/9/9
// */
//@ApiModel(description = "响应状态")
//public class CommonHttpCode {
//
//    public CommonHttpCode() {
//        Okv kv = Okv.by(200, "请求成功");
//
//        this.httpCodeKv = kv;
//    }
//
//    private Okv httpCodeKv;
//
//    public Okv getHttpCodeKv() {
//        return httpCodeKv;
//    }
//
//    public void setHttpCodeKv(Okv httpCodeKv) {
//        this.httpCodeKv = httpCodeKv;
//    }
//}
