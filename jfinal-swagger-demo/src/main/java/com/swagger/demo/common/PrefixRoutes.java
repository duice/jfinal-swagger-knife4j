package com.swagger.demo.common;

import com.jfinal.config.Routes;
import com.jfinal.core.Controller;

/**
 * @author: lbq
 * @email: 526509994@qq.com
 * @date: 2020/12/8
 */
public class PrefixRoutes extends Routes {
    private String apiPath;
    private String basePackage;

    public PrefixRoutes(String apiPath, String basePackage) {
        this.apiPath = apiPath;
        this.basePackage = basePackage;
    }

    @Override
    public Routes add(String controllerPath, Class<? extends Controller> controllerClass, String viewPath) {
        String cp = apiPath + controllerPath;
        return super.add(cp, controllerClass, viewPath);
    }

    @Override
    public void config() {
        this.scan(basePackage);
    }
}
