//package com.swagger.demo.common.swagger;
//
//import com.lastb7.swagger.annotation.ApiField;
//import com.lastb7.swagger.annotation.ApiModel;
//import com.lastb7.swagger.enumeration.ApiResEnum;
//
///**
// * 参考AntDesign约定的Json返回格式
// * https://beta-pro.ant.design/docs/request-cn
// *
// * @author: lbq
// * 联系方式: 526509994@qq.com
// * 创建日期: 2020/9/8
// */
//@ApiModel(description = "响应结果")
//public class CommonRes {
//
//    @ApiField(description = "请求状态", example = "ok", exampleEnum = {"ok", "fail"})
//    private String state;
//
//    @ApiField(description = "调用时间", example = "yyyy-MM-dd HH:mm:ss")
//    private String timestamp;
//
//    @ApiField(description = "错误码(仅异常返回)", example = "-1", type = ApiResEnum.INTEGER)
//    private int code;
//
//    @ApiField(description = "错误信息(仅异常返回)", example = "错误提示")
//    private String message;
//
//    @ApiField(description = "错误类型(仅异常返回)", example = "biz", exampleEnum = {"biz:业务错误", "other:运行时异常"})
//    private int type;
//
//    public String getState() {
//        return state;
//    }
//
//    public void setState(String state) {
//        this.state = state;
//    }
//
//    public String getTimestamp() {
//        return timestamp;
//    }
//
//    public void setTimestamp(String timestamp) {
//        this.timestamp = timestamp;
//    }
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public String getMessage() {
//        return message;
//    }
//
//    public void setMessage(String message) {
//        this.message = message;
//    }
//
//    public int getType() {
//        return type;
//    }
//
//    public void setType(int type) {
//        this.type = type;
//    }
//}
